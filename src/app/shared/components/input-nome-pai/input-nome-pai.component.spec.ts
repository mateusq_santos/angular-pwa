import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputNomePaiComponent } from './input-nome-pai.component';

describe('InputNomePaiComponent', () => {
  let component: InputNomePaiComponent;
  let fixture: ComponentFixture<InputNomePaiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputNomePaiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputNomePaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
