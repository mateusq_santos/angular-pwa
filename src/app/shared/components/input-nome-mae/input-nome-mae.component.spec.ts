import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputNomeMaeComponent } from './input-nome-mae.component';

describe('InputNomeMaeComponent', () => {
  let component: InputNomeMaeComponent;
  let fixture: ComponentFixture<InputNomeMaeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputNomeMaeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputNomeMaeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
