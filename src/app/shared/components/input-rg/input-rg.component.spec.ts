import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputRgComponent } from './input-rg.component';

describe('InputRgComponent', () => {
  let component: InputRgComponent;
  let fixture: ComponentFixture<InputRgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputRgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputRgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
