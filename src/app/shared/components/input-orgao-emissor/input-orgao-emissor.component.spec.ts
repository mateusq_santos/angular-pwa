import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputOrgaoEmissorComponent } from './input-orgao-emissor.component';

describe('InputOrgaoEmissorComponent', () => {
  let component: InputOrgaoEmissorComponent;
  let fixture: ComponentFixture<InputOrgaoEmissorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputOrgaoEmissorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputOrgaoEmissorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
