import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputDataEmissaoComponent } from './input-data-emissao.component';

describe('InputDataEmissaoComponent', () => {
  let component: InputDataEmissaoComponent;
  let fixture: ComponentFixture<InputDataEmissaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputDataEmissaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDataEmissaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
