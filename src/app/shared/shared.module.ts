import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputNomeComponent } from './components/input-nome/input-nome.component';
import { InputCnpjComponent } from './components/input-cnpj/input-cnpj.component';
import { InputCpfComponent } from './components/input-cpf/input-cpf.component';
import { InputNomeMaeComponent } from './components/input-nome-mae/input-nome-mae.component';
import { InputNomePaiComponent } from './components/input-nome-pai/input-nome-pai.component';
import { InputDataEmissaoComponent } from './components/input-data-emissao/input-data-emissao.component';
import { InputOrgaoEmissorComponent } from './components/input-orgao-emissor/input-orgao-emissor.component';
import { InputEmailComponent } from './components/input-email/input-email.component';
import { InputRgComponent } from './components/input-rg/input-rg.component';
import { InputDataNascimentoComponent } from './components/input-data-nascimento/input-data-nascimento.component';
import { MaterialAngularModule } from './material-angular/material-angular.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { InputSenhaComponent } from './components/input-senha/input-senha.component';
import { InputUsuarioComponent } from './components/input-usuario/input-usuario.component';

@NgModule({
  declarations: [
    InputNomeComponent,
    InputCnpjComponent,
    InputCpfComponent,
    InputNomeMaeComponent,
    InputNomePaiComponent,
    InputDataEmissaoComponent,
    InputOrgaoEmissorComponent,
    InputEmailComponent,
    InputRgComponent,
    InputDataNascimentoComponent,
    InputSenhaComponent,
    InputUsuarioComponent,
  ],
  imports: [
    CommonModule,
    MaterialAngularModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule,
  ],
  exports: [
    MaterialAngularModule,
    InputNomeComponent,
    InputCnpjComponent,
    InputCpfComponent,
    InputNomeMaeComponent,
    InputNomePaiComponent,
    InputDataEmissaoComponent,
    InputOrgaoEmissorComponent,
    InputEmailComponent,
    InputRgComponent,
    InputDataNascimentoComponent,
    InputSenhaComponent,
    InputUsuarioComponent,
  ],
})
export class SharedModule {}
